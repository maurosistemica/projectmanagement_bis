package DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import bean.Project;
import utils.HibernateUtils;

public class ProjectDAO {
	
	private HibernateUtils hibernateUtils  = new HibernateUtils();
	
	public List<Project> getProjectList(){
		
		List<Project> projectList = new ArrayList<Project>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("from Project");
			
			if(!CollectionUtils.isEmpty(query.list())) {
				projectList = (List<Project>) query.list();
			} 
			
			tx.commit();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return projectList;
		
	}

	public Project getProjectByProjectCode(String projectCode){
		
		Project project = new Project();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("from Project where projectCode = :projectCode");
			query.setParameter("projectCode", projectCode);
			
			if(!CollectionUtils.isEmpty(query.list())) {
				project = (Project) query.uniqueResult();
			} 
		
			tx.commit();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		
		return project;
		
	}

	public Boolean projectExists(String projectCode) {

		Boolean alreadyExists = false;
		
		List<Project> projectList = new ArrayList<Project>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("from Project p where lower(p.projectCode) = :projectCode");
			query.setParameter("projectCode", projectCode.toLowerCase());
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				projectList = (List<Project>) query.list();
			} 
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		if(!CollectionUtils.isEmpty(projectList)) {
			alreadyExists = true;
		}
		
		return alreadyExists;
	}
	
	public void updateProject(Project project) throws Exception {
		
		Session session = hibernateUtils.currentSession();
		Transaction tx;
		
		try {
			tx = session.beginTransaction();
			
			session.update(project);
			
			//aggiungere write tabella log
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			session.clear();
			throw e;
		} finally {
			session.clear();
			session.close();
		}		
	}
	
	public void saveProject(Project project) throws Exception {
		
		Session session = hibernateUtils.currentSession();
		Transaction tx;
		
		try {
			tx = session.beginTransaction();
			
			session.save(project);
			
			//aggiungere write tabella log
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			session.clear();
			throw e;
		} finally {
			session.clear();
			session.close();
		}		
	}
	
	public void deleteProjectByProjectCode(String projectCode) throws Exception {
		
		Session session = hibernateUtils.currentSession();
		Transaction tx;
		
		try {
			tx = session.beginTransaction();
			
			Project deletedProject = (Project) session.load(Project.class, projectCode);
			
			session.delete(deletedProject);
			
			//aggiungere write tabella log
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			session.clear();
			throw e;
		} finally {
			session.clear();
			session.close();
		}		
	}

}
