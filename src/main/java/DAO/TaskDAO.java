package DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import bean.Project;
import bean.Task;
import utils.HibernateUtils;

public class TaskDAO {

	HibernateUtils hibernateUtils = new HibernateUtils();
	
	public List<Task> getTaskList() {
		
		List<Task> tasksList = new ArrayList<Task>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
			Query query = session.createQuery("from Task");
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				tasksList = (List<Task>) query.list();
			} 
			
			tx.commit();
	
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	
		return tasksList;
	}

	public Task getTaskByProjectCodeAndTaskCode(String projectCode,String taskCode) {
		
		Task task = new Task();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("select distinct t from Task t join t.projectList pjt where pjt.projectCode = :projectCode and t.taskCode = :taskCode");
			query.setParameter("projectCode", projectCode);
			query.setParameter("taskCode", taskCode);
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				task = (Task) query.uniqueResult();
			} 
			
			tx.commit();
	
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return task;
	}
	
	public List<Task> getTasksByProjectCode(String projectCode) {
		
		List<Task> taskList = new ArrayList<Task>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("select distinct t from Task t join t.projectList pjt where pjt.projectCode = :projectCode");
			query.setParameter("projectCode", projectCode);
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				taskList = (List<Task>) query.list();
			} 
			
			tx.commit();
	
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	
		return taskList;
	}
	
	public Boolean taskExists(String taskCode) {

		Boolean alreadyExists = false;
		
		List<Task> taskList = new ArrayList<Task>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("from Task t where lower(t.taskCode) = :taskCode");
			query.setParameter("taskCode", taskCode.toLowerCase());
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				taskList = (List<Task>) query.list();
			} 
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		if(!CollectionUtils.isEmpty(taskList)) {
			alreadyExists = true;
		}
		
		return alreadyExists;
	}
	
	public void updateTask(Task Task) throws Exception {
		
		Session session = hibernateUtils.currentSession();
		Transaction tx;
		
		try {
			tx = session.beginTransaction();
			
			session.update(Task);
			
			//aggiungere write tabella log
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			session.clear();
			throw e;
		} finally {
			session.clear();
			session.close();
		}		
	}
	
	public void saveTask(Task task) throws Exception {
		
		Session session = hibernateUtils.currentSession();
		Transaction tx;
		
		try {
			tx = session.beginTransaction();
			
			session.save(task);
			
			//aggiungere write tabella log
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			session.clear();
			throw e;
		} finally {
			session.clear();
			session.close();
		}		
	}
	
}
