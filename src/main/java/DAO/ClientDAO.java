package DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import bean.Client;
import utils.HibernateUtils;

public class ClientDAO {

	HibernateUtils hibernateUtils = new HibernateUtils();

	public List<Client> getClientList() {
		
		List<Client> clientList = new ArrayList<Client>();
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
			
			Query query = session.createQuery("from Client");
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				clientList = (List<Client>) query.list();
			} 
		
			tx.commit();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return clientList;
	}

	public Client getClientByProjectCodeAndClientCode(String projectCode,String clientCode) {
		
		Client client = new Client();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
			
			Query query = session.createQuery("select distinct c from Client c join c.projectList pjt where pjt.projectCode = :projectCode and t.clientCode = :clientCode");
			query.setParameter("projectCode", projectCode);
			query.setParameter("clientCode", clientCode);
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				client = (Client) query.uniqueResult();
			} 
		
			tx.commit();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return client;
	}
	
	public List<Client> getClientsByProjectCode(String projectCode) {
		
		List<Client> clientList = new ArrayList<Client>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
			
			Query query = session.createQuery("select distinct c from Client c join c.projectList pjt where pjt.projectCode = :projectCode");
			query.setParameter("projectCode", projectCode);
	
			if(CollectionUtils.isNotEmpty(query.list())) {
				clientList = (List<Client>) query.list();
			} 
		
			tx.commit();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
				
		return clientList;
	}
}
