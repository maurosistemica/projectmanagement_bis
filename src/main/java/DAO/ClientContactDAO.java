package DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import bean.ClientContact;
import utils.HibernateUtils;

public class ClientContactDAO {

	HibernateUtils hibernateUtils = new HibernateUtils();
	
	public List<ClientContact> getClientContactsByClientCode(String clientCode) {
		
		List<ClientContact> clientContactList = new ArrayList<ClientContact>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("select distinct ccl from Client c join c.clientContactList ccl where ccl.clientCode = :clientCode");
			query.setParameter("clientCode", clientCode);
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				clientContactList = (List<ClientContact>) query.list();
			} 
		
			tx.commit();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		
		return clientContactList;
	}
	
	public void updateClientContact(ClientContact clientContact) {
		
		Session session = hibernateUtils.currentSession();
		
		session.update(clientContact);
		
		hibernateUtils.closeSession();
	}
	
}
