package DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import bean.Project;
import bean.User;
import utils.HibernateUtils;

public class UserDAO {

	HibernateUtils hibernateUtils = new HibernateUtils();
	
	public List<User> getUserList() {
		
		List<User> usersList = new ArrayList<User>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("from User");
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				usersList = (List<User>) query.list();
			} 
			
			tx.commit();
		
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return usersList;
	}

	public List<Project> getProjectsByUsername(String username) {
		
		List<Project> projectList = new ArrayList<Project>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("select distinct pjt from User u join u.projectList pjt where upper(u.userCode) = :username");
			query.setParameter("username", username.toUpperCase());
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				projectList = (List<Project>) query.list();
			} 
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return projectList;
	}
	
	public List<User> getUsersByProjectCode(String projectCode) {
		
		List<User> userList = new ArrayList<User>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("select distinct u from User u join u.projectList pjt where pjt.projectCode = :projectCode");
			query.setParameter("projectCode", projectCode);
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				userList = (List<User>) query.list();
			} 
			
			tx.commit();
		
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return userList;
	}
	
}
