package utils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import bean.User;

public class JWTUtils {

//	Key key = MacProvider.generateKey();
	
	private static final String CONFIG_PROPERTIES_URL = "src/main/resources/config.properties"; 
	
	public byte[] getJWTKey() {
		
		Properties prop = new Properties();
		InputStream input = null;

		try {
			
			input = new FileInputStream(CONFIG_PROPERTIES_URL);

			prop.load(input);

			return prop.getProperty("jwt_key").getBytes("UTF-8");
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("token key not found");
			return new byte[0];
		}	
	}
	
//	public String createJWT(User user) {
//
//		String jws = Jwts.builder()
//				  .setIssuer("Stormpath")
//				  .setSubject(user.getUsername())
//				  .claim("username", user.getUsername())
//				  .claim("role", user.getRole())
//				  .setIssuedAt(new Date())
//				  .signWith(SignatureAlgorithm.HS256, key).compact();
//		
//		return jws;
//		
//	}
	
	public User parseJWT(String jwt) {
		
		User user = new User();
		
		Jwt<Header, Claims> claims;
		
		try {
			
			claims = Jwts.parser()
					.setSigningKey(getJWTKey())
					.parseClaimsJwt(jwt.substring(0, jwt.lastIndexOf(".")+1));
			
//			JwsHeader header = claims.getHeader();
//			String signature = claims.getSignature();
			
			String username = claims.getBody().get("userCode") != null ? claims.getBody().get("userCode").toString() : "";
			String role = claims.getBody().get("roleCode") != null ? claims.getBody().get("roleCode").toString() : "";
			Boolean isAdmin = claims.getBody().get("isAdmin") != null ? (Boolean) claims.getBody().get("isAdmin") : false;
			
			user.setUserCode(username);
			user.setRoleCode(role);
			user.setIsAdmin(isAdmin);
			
		} catch(Exception e) {
			System.out.println("Errore parsing JWT");
		}
		
		return user;
	}
	   
}
