package bean;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="projectCode")
public class Project {
	
	private String projectCode;
	private String projectName;
	
	private Client client;
	private String address;
	private Integer budget;
	private String budgetType;
	private Date startDate;
	private Date endDate;

	private Set<User> userList = new HashSet<User>(0);
	private Set<Task> taskList = new HashSet<Task>(0);
	
	public Project(String projectCode, String projectName, Client client, String address, Integer budget,
			String budgetType, Date startDate, Date endDate, Set<User> userList, Set<Task> taskList) {
		super();
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.client = client;
		this.address = address;
		this.budget = budget;
		this.budgetType = budgetType;
		this.startDate = startDate;
		this.endDate = endDate;
		this.userList = userList;
		this.taskList = taskList;
	}

	public Project() {
		super();
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	@XmlElement(name="client")
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getBudget() {
		return budget;
	}

	public void setBudget(Integer budget) {
		this.budget = budget;
	}

	public String getBudgetType() {
		return budgetType;
	}

	public void setBudgetType(String budgetType) {
		this.budgetType = budgetType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@XmlElement(name="userList")
	public Set<User> getUserList() {
		return userList;
	}

	public void setUserList(Set<User> userList) {
		this.userList = userList;
	}

	@XmlElement(name="taskList")
	public Set<Task> getTaskList() {
		return taskList;
	}

	public void setTaskList(Set<Task> taskList) {
		this.taskList = taskList;
	}
	
}
