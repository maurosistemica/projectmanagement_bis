package bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="contactName")
public class ClientContact implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Client client;
	private String contactName;
	private String phoneNumber;
	private String email;
	
	public ClientContact(Client client, String contactName, String phoneNumber, String email) {
		super();
		this.client = client;
		this.contactName = contactName;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}

	public ClientContact() {
		super();
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
