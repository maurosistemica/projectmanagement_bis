package bean;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="clientCode")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Client {

	private String clientCode;
	private String clientName;
	private String address;
	
	private Set<ClientContact> clientContactList = new HashSet<ClientContact>(0);
	
	private Set<Project> projectList = new HashSet<Project>(0);
	
	public Client(String clientCode, String clientName, String address) {
		super();
		this.clientCode = clientCode;
		this.clientName = clientName;
		this.address = address;
	}

	public Client() {
		super();
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Set<ClientContact> getClientContactList() {
		return clientContactList;
	}

	public void setClientContactList(Set<ClientContact> clientContactList) {
		this.clientContactList = clientContactList;
	}

	public Set<Project> getProjectList() {
		return projectList;
	}

	public void setProjectList(Set<Project> projectList) {
		this.projectList = projectList;
	}
	
}
