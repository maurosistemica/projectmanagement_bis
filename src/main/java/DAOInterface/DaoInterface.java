package DAOInterface;

import java.util.List;

public interface DaoInterface<T,U> {
		
	List<T> find(U... args);
	
	void delete(U param);
	
	void update(U param);

}
