package rest.serviceInterface;

import java.util.List;

public interface ServiceInterface<T,U> {
		
	List<T> find(U... args);
	
	void delete(U param);
	
	void update(U param);

}

