package rest.services;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("api")
public class ProjectManagementApp extends ResourceConfig {
    public ProjectManagementApp() {
        packages("rest.services");
    }
}