package rest.services;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.commons.collections.CollectionUtils;
import org.json.JSONObject;

import DAO.ClientDAO;
import bean.Project;
import bean.Client;

@Path("/clients")
public class ClientService {
	
	ClientDAO clientDao = new ClientDAO();
	
	@GET
	@Produces({"application/xml","application/json"})
	public Response getClientList() {
		
		List<Client> clientList = clientDao.getClientList();
 
		return Response.status(200).entity(clientList).build();
		
	}
}
