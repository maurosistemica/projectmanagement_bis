package rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import DAO.TaskDAO;
import bean.Task;

@Path("/tasks")
public class TaskService {
	
	TaskDAO taskDao = new TaskDAO();
	
	@Context
    private UriInfo uriInfo;
	
	@GET
	@Produces({"application/xml","application/json"})
	public Response getTaskList() {
		
		List<Task> taskList = taskDao.getTaskList();
 
		return Response.status(200).entity(taskList).build();
		
	}
	
	@Path("{task_code}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response udpateTask(Task task, @PathParam("task_code") String taskCode) {
		
		Boolean alreadyExists = taskDao.taskExists(taskCode);
		
		if(alreadyExists) {
			try {
				taskDao.updateTask(task);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return Response.created(uriInfo.getAbsolutePath()).build();
			
		} else {		
			return Response.noContent().build();
			
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveTask(Task task) {
		
		Boolean alreadyExists = taskDao.taskExists(task.getTaskCode());
		
		if(!alreadyExists) {
			try {
				taskDao.saveTask(task);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return Response.created(uriInfo.getAbsolutePath()).build();
			
		} else {		
			return Response.noContent().build();
			
		}
	}

}
