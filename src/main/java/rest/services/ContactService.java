package rest.services;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.commons.collections.CollectionUtils;
import org.json.JSONObject;

import DAO.ClientContactDAO;
import bean.ClientContact;

@Path("/projects/{project_code}/clients/{client_code}/contacts")
public class ContactService {

	ClientContactDAO clientContactDao = new ClientContactDAO();
	
	@GET
	@Produces({"application/xml","application/json"})
	public Response getClientByProjectCode(@PathParam("client_code") String clientCode) {
		
		try {
			clientCode = URLDecoder.decode(clientCode,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		List<ClientContact> clientContactList = clientContactDao.getClientContactsByClientCode(clientCode);
		
		JSONObject jsonObject = new JSONObject();
		
		if(CollectionUtils.isNotEmpty(clientContactList)) {
			for(ClientContact cc : clientContactList) {
				jsonObject.put(cc.getContactName(),cc.getClient().getClientCode());
			}
		}
		
		return Response.status(200).entity(jsonObject.toMap()).build();
	}
			
}
