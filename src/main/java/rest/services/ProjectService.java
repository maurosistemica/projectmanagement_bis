package rest.services;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.collections.CollectionUtils;
import org.json.JSONObject;

import DAO.ProjectDAO;
import DAO.TaskDAO;
import bean.Project;
import bean.Task;
import bean.User;

@Path("/projects")
public class ProjectService {
	
	@Context
    private UriInfo uriInfo;
	
	ProjectDAO projectDao = new ProjectDAO();
	
	TaskDAO taskDao = new TaskDAO();
	
	@Path("{project_code}")
	@GET
	@Produces({"application/xml","application/json"})
	public Response getProjectsByProjectCode(@PathParam("project_code") String projectCode) {
		
		try {
			projectCode = URLDecoder.decode(projectCode,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		Project project = projectDao.getProjectByProjectCode(projectCode);
		
		JSONObject jsonObject = new JSONObject();
		
		if(project != null && project.getProjectCode() != null && project.getProjectName() != null) {
			jsonObject.put(project.getProjectCode(),project.getProjectName());
		}
		
		return Response.status(200).entity(jsonObject.toMap()).build();
	}
	
	@GET
	@Produces({"application/xml","application/json"})
	public Response getProjectList() {
		
		List<Project> projectList = projectDao.getProjectList();
 
		return Response.status(200).entity(projectList).build();
		
	}
	
	@Path("{project_code}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response udpateProject(Project project, @PathParam("project_code") String projectCode) {
		
		Boolean alreadyExists = projectDao.projectExists(projectCode);
		
		if(alreadyExists) {
			try {
				projectDao.updateProject(project);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return Response.created(uriInfo.getAbsolutePath()).build();
			
		} else {		
			return Response.noContent().build();
			
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveProject(Project project) {
		
		Boolean alreadyExists = projectDao.projectExists(project.getProjectCode());
		
		if(!alreadyExists) {
			try {
				projectDao.saveProject(project);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return Response.created(uriInfo.getAbsolutePath()).build();
			
		} else {		
			return Response.noContent().build();
			
		}
	}
	
	
	@Path("{project_code}")
	@DELETE
	public Response deleteUser(@PathParam("project_code") String projectCode) {
		
		try {
			projectDao.deleteProjectByProjectCode(projectCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.created(uriInfo.getAbsolutePath()).build();
	}
	
	
	@Path("{project_code}/tasks/{task_code}")
	@GET
	@Produces({"application/xml","application/json"})
	public Response getTaskByProjectCodeAndTaskCode(@PathParam("project_code") String projectCode,@PathParam("task_code") String taskCode) {
		
		try {
			projectCode = URLDecoder.decode(projectCode,"UTF-8");
			taskCode = URLDecoder.decode(taskCode,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		Task task = taskDao.getTaskByProjectCodeAndTaskCode(projectCode,taskCode);
		
		JSONObject jsonObject = new JSONObject();
		
		if(task != null) {
			jsonObject.put(task.getTaskCode(),task.getTaskName());
		}
		
		return Response.status(200).entity(jsonObject.toMap()).build();
	}
	
	@Path("{project_code}/tasks")
	@GET
	@Produces({"application/xml","application/json"})
	public Response getTasksByProjectCode(@PathParam("project_code") String projectCode) {
		
		try {
			projectCode = URLDecoder.decode(projectCode,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		List<Task> taskList = taskDao.getTasksByProjectCode(projectCode);
		
		return Response.status(200).entity(taskList).build();
	}

}
