package rest.services;

import java.util.List;

import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.collections.CollectionUtils;
import org.json.JSONObject;

import DAO.UserDAO;
import bean.Project;
import bean.User;
import utils.JWTUtils;

@Path("/users")
public class UserService {
	
	UserDAO userDao = new UserDAO();
	
	@Path("{userToken}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
//	@JWTTokenNeeded
	public List<Project> getUsersByProjectCodeAndUsername(@PathParam("userToken") @Encoded String userToken) {
		
		JWTUtils jwtUtils = new JWTUtils();
		
		User user = jwtUtils.parseJWT(userToken);

		List<Project> projectList = userDao.getProjectsByUsername(user.getUserCode());
		return projectList;
		
	}
	
	@GET
	@Produces({"application/xml","application/json"})
	public Response getProjectList() {
		
		List<User> userList = userDao.getUserList();
		
		JSONObject jsonObject = new JSONObject();
		
		if(CollectionUtils.isNotEmpty(userList)) {
			for (User user : userList) {
				if(CollectionUtils.isNotEmpty(user.getProjectList())) {
					for (Project pjt : user.getProjectList()) {
						jsonObject.put(pjt.getProjectCode(),user.getUserCode());
					}
				}
			}
		}
 
		return Response.status(200).entity(jsonObject.toMap()).build();
		
	}

}
